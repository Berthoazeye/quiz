

export const Qcm = {
    Auteur: "AZEYE Romeo Bertho",
    Application: "Bertho-Quizz",
    quiz: {

        Reseaux: {

            Facile: [
                {
                    id: 0,
                    question: "A quoi peut-on assimiler un protocole \n dans un reseaux informatique ?",
                    options: [
                        "Une ligne de communication",
                        "Une Adresse IP ",
                        "Un langage",  
                        "Aucun"
                    ],
                    reponse: "Un langage"
                },
                {
                    id: 1,
                    question: "Quelle est la fonctionnalité assurée par la couche session du modèle OSI ?",
                    options: [
                        "La segmentation",
                        "Le routage ",
                        "Synchronisation des communications",
                        "Transfère des données"
                    ],
                    reponse: "Synchronisation des communications"
                },
                {
                    id: 3,
                    question: "Lequel des éléments suivants n'est pas un protocole reseaux",
                    options: [
                        "IPX/SPX",
                        "DHCP",
                        "NetBEUT",
                        "Aucun"
                    ],
                    reponse: "Aucun"
                },
                {
                    id: 4,
                    question: "Lequel des éléments suivants désigne un protocole reseaux non routable sur internet",
                    options: [
                        "IPX/SPX",
                        "DHCP",
                        "NetBEUT",
                        "Aucun"
                    ],
                    reponse: "NetBEUT"
                },
                {
                    id: 5,
                    question: "À quoi sert ARP ?",
                    options: [
                        "À trouver l'adresse IP d'une station dont on connaît l'adresse MAC",
                        "À trouver l'adresse MAC d'une station dont on connaît l'adresse IP",
                        "À trouver l'adresse MAC d'une station dont on connaît le nom de HOST",
                        "Aucun"
                    ],
                    reponse: "À trouver l'adresse MAC d'une station dont on connaît l'adresse IP"
                },
                {
                    id: 6,
                    question: "Lors d'une première communication entre deux équipements réseau à travers un switch, quel(s) est (sont) lets) protocole(s) mobilisé(s) ?",
                    options: [
                        "ARP et ICMP",
                        "ARP et IP",
                        "ARP et STP",
                        "Aucun"
                    ],
                    reponse: "ARP et ICMP"
                },
                {
                    id: 7,
                    question: "Dans quel type de réseau peut-on classifier le réseau téléphonique cellulaire ?",
                    options: [
                        "WAN",
                        "LAN",
                        "MAN",
                        "Aucun"
                    ],
                    reponse: "WAN"
                },
                {
                    id: 8,
                    question: "Lequel des réseaux suivants implémente-t-il une technique de transfert de données par commutation de circuits?",
                    options: [
                        "Réseau Internet",
                        "Réseau téléphonique commuté",
                        "Réseau de capteurs",
                        "Cloud"
                    ],
                    reponse: "Réseau téléphonique commuté"
                },
                {
                    id: 9,
                    question: "Laquelle des techniques de commutation suivante prend telle en charge les applications Cellule multimédia par rapport aux autres ?",
                    options: [
                        "cellule",
                        "message",
                        "paquets",
                        "Aucune"
                    ],
                    reponse: "cellule"
                },
                {
                    question: "Quelle est la taille minimale d'une trame dans la technologie Ethernet?",
                    options: [
                        "18 octets",
                        "64 octets",
                        "128 octets",
                        "512 octets"
                    ],
                    reponse: "64 octets"
                },
            ],
            Intermediaire: [
                {
                    id: 0,
                    question: "",
                    options: [
                        "la segmentation",
                        "",
                        "",
                        ""
                    ],
                    reponse: 0
                },
                {
                    id: 0,
                    question: "ffjkjdfkjskjksjdkjskdjk",
                    options: [
                        "la segmentation",
                        "",
                        "",
                        ""
                    ],
                    reponse: 0
                }
            ],
            Difficile: [
                {
                    id: 0,
                    question: 5,
                    options: [
                        "la segmentation",
                        "",
                        "",
                        ""
                    ],
                    reponse: 0
                }
            ]

        },

        Programmation: {
            Facile: [
                {
                    id: 0,
                    question: "python est :",
                    options: [
                        "Un langage de programmation",
                        "Un langage de Script",
                        "",
                        ""
                    ],
                    reponse: "Un langage de Script"
                },
                {
                    id: 0,
                    question: "python est :",
                    options: [
                        "Un langage de programmation",
                        "Un langage de Script",
                        "",
                        ""
                    ],
                    reponse: "Un langage de Script"
                },
                {
                    id: 0,
                    question: "python est :",
                    options: [
                        "Un langage de programmation",
                        "Un langage de Script",
                        "",
                        ""
                    ],
                    reponse: "Un langage de Script"
                },
                {
                    id: 0,
                    question: "python est :",
                    options: [
                        "Un langage de programmation",
                        "Un langage de Script",
                        "",
                        ""
                    ],
                    reponse: "Un langage de Script"
                },
                {
                    id: 0,
                    question: "python est :",
                    options: [
                        "Un langage de programmation",
                        "Un langage de Script",
                        "",
                        ""
                    ],
                    reponse: "Un langage de Script"
                },
                {
                    id: 0,
                    question: "python est :",
                    options: [
                        "Un langage de programmation",
                        "Un langage de Script",
                        "",
                        ""
                    ],
                    reponse: "Un langage de Script"
                },
                {
                    id: 0,
                    question: "python est :",
                    options: [
                        "Un langage de programmation",
                        "Un langage de Script",
                        "",
                        ""
                    ],
                    reponse: "Un langage de Script"
                },
                {
                    id: 0,
                    question: "python est :",
                    options: [
                        "Un langage de programmation",
                        "Un langage de Script",
                        "",
                        ""
                    ],
                    reponse: "Un langage de Script"
                },
                {
                    id: 0,
                    question: "python est :",
                    options: [
                        "Un langage de programmation",
                        "Un langage de Script",
                        "",
                        ""
                    ],
                    reponse: "Un langage de Script"
                },
                {
                    id: 0,
                    question: "python est :",
                    options: [
                        "Un langage de programmation",
                        "Un langage de Script",
                        "",
                        ""
                    ],
                    reponse: "Un langage de Script"
                },
            ],
            Intermediaire: [
                {
                    id: 0,
                    question: "",
                    options: [
                        "la segmentation",
                        "",
                        "",
                        ""
                    ],
                    reponse: 0
                }
            ],
            Difficile: [
                {
                    id: 0,
                    question: 5,
                    options: [
                        "la segmentation",
                        "",
                        "",
                        ""
                    ],
                    reponse: 0
                }
            ]
        },
        C: {
            Facile: [
                {
                    id: 0,
                    question: "jhjshdjshdhsh",
                    options: [
                        "la segmentation",
                        "",
                        "",
                        ""
                    ],
                    reponse: 0
                }
            ],
            Intermediaire: [
                {
                    id: 0,
                    question: "",
                    options: [
                        "la segmentation",
                        "",
                        "",
                        ""
                    ],
                    reponse: 0
                }
            ],
            Difficile: [
                {
                    id: 0,
                    question: 5,
                    options: [
                        "la segmentation",
                        "",
                        "",
                        ""
                    ],
                    reponse: 0
                }
            ]
        }
    },
}