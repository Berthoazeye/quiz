import firebase from "../firebase";
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom'
// import { GoogleLogin } from 'react-google-login';


function Login(props) {

    const c = useNavigate()
    // eslint-disable-next-line no-unused-vars
    const redirection = (user) => {
        c('/acceuil', user);
    }
    const firebas = firebase.auth();
    const data = {
        email: "",
        password: ''
    }
    const [signdata, setSingndata] = useState(data);
    const { error, setError } = useState('')
    const { spinnerButton, setSpinnerButton } = useState(false)
    const { email, password } = signdata

    const handelChange = (e) => {
        setSingndata({ ...signdata, [e.target.id]: e.target.value })
        console.log(signdata)
    }
    const handelsubmit = (e) => {
        console.log(signdata)
        e.target.classList.add('was-validated')
        e.preventDefault();

        // console.log(firebas.signInWithEmailAndPassword("emaill", "passwordd"));

        firebas.signInWithEmailAndPassword(email, password)
            .then(user => {
                setSingndata({ ...data });
                redirection(user)
                console.log(user);
                console.log('login okkkkkkkkkk');
                // props.history.push('/welcome')
                //   Auth.login();
            })
            .catch(error => {
                //   seterror(error);s
                const errorCode = error.code;
                if (errorCode === "auth/network-request-failed") {
                    setError("veillez vous connecter sur Internet")
                    c(0, "veillez vous connecter sur Internet");

                    console.log('errorCode')
                }
                if (errorCode === "auth/email-already-in-use") {
                    setError("votre addresse email existe deja")
                    c(0, "votre addresse email existe deja");
                    // console.log('errorCode')
                }
                console.log(error);
                setSingndata({ ...data })

            })
    }
    return (
        <div className="modal modal-signin  d-none bg-none py-0" tabIndex="-1" role="dialog" id="modalSignin">
            <div className="modal-dialog" role="document">
                <div className="modal-content rounded-5 shadow">
                    <div className="modal-header p-5 pb-4 border-bottom-0">

                        <h3 className="fw-bold mb-0">Se connecter </h3>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" id='close'></button>
                    </div>

                    <div className="modal-body p-5 pt-0">
                        <form className="" action='/acceuil' method='get' id="essai" onSubmit={handelsubmit} >
                            <div className="form-floating mb-3">
                                <input type="email" className="form-control rounded-4" value={email} onChange={handelChange} id="email" required placeholder="name@example.com" />
                                <label htmlFor="email">Email address</label>
                            </div>

                            <div className="form-floating mb-3">
                                <input type="password" className="form-control rounded-4" value={password} onChange={handelChange} id="password" maxLength={8} minLength={8} required placeholder="Password" />
                                <label htmlFor="password">Password</label>
                                <div className="invalid-feedback">
                                    Le mot de pass doit avoir au moins 8 caractère.
                                </div>
                            </div>

                            {!spinnerButton ?
                                <button className="w-100 mb-2 btn btn-lg rounded-4 btn-primary" type="submit" id='submitButtom'>Connexion</button>
                                :
                                <button className="w-100 mb-2 btn btn-lg rounded-4 btn-primary" type="submit" id='submitButtom' disabled>
                                    <span className="spinner-border spinner-border-sm" style={{ width: "2rem", height: "2rem" }} role="status" aria-hidden="true"></span>
                                    <span className=""> Loading...</span>
                                </button>}
                            <br />
                            <small className="text-muted">En cliquant sur <b>Connexion</b> vous accepter les conditions d'utilisation.</small>
                            <hr className="my-4" />
                            <h2 className="fs-5 fw-bold mb-3"> Ou se connecter avec :</h2>
                            <button className="w-100 py-2 mb-2 btn btn-outline-dark rounded-4" type="submit">
                                {/* <svg className="bi me-1" width="16" height="16"><use xlink:href="#twitter"/></svg> */}
                                {/* <GoogleLogin

                                    buttonText="Sign in with Google"
                                    onSuccess={console.log("nbdsbnd")}
                                    onFailure={console.log("nbdsbnd")}
                                    cookiePolicy={'single_host_origin'}
                                    isSignedIn={true}
                                /> */}
                            </button>
                            <button className="w-100 py-2 mb-2 btn btn-outline-primary rounded-4" type="submit">
                                {/* <svg className="bi me-1" width="16" height="16"><use xlink:href="#facebook"/></svg> */}
                                Se connecter avec Facebook
                            </button>
                            <button className="w-100 py-2 mb-2 btn btn-outline-secondary rounded-4" type="submit">
                                {/* <svg className="bi me-1" width="16" height="16"><use xlink:href="#github"/></svg> */}
                                Se connecter avec GitHub
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Login;