import React from 'react';
import "../assets/dist/css/bootstrap.min.css";
import logo from '../logo.svg';
import { Link } from "react-router-dom"

// import { Button, Stack, Navbar, Nav, Container, NavDropdown } from 'react-bootstrap';
// import Buttons from 'react-bootstrap/Button';

function Header(props) {
  // console.log(props)
  return (
    <div >
      <main >
        <header>
          <div className="px-3 py-2 bg-dark text-white">
            <div className="container">
              <div className="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
                <a href="/" className="d-flex align-items-center my-4 my-lg-0 me-lg-auto text-white text-decoration-none" >
                  {/* <svg className="bi me-2" width="40" height="32" role="img" aria-label="Bootstrap"><use xlink:href="#bootstrap"/></svg> */}
                  <img src={logo} className="" alt="logo" />Bertho-Quizz
                </a>

                <ul className="nav col-12 col-lg-auto my-2 justify-content-center my-md-0 text-small">
                  <li>
                    <Link to="/acceuil" className="nav-link text-primary">
                      Home
                    </Link>

                  </li>
                  <li>

                    <Link to="/admin" className="nav-link text-white">
                      Dashboard
                    </Link>
                  </li>
                  <li>

                    <Link to="/admin" className="nav-link text-white">
                      Admin
                    </Link>
                  </li>

                </ul>
              </div>
            </div>
          </div>
          <div className="px-3 py-2 border-bottom mb-3">
            <div className="container d-flex flex-wrap justify-content-center">
              <form className="col-12 col-lg-auto mb-2 mb-lg-0 me-lg-auto">
                <input type="search" className="form-control" placeholder="Search..." aria-label="Search" />
              </form>
              {props.children}
            </div>
          </div>
        </header>

        <div className="b-example-divider"></div>
      </main>
      {/*  
      je suis l'entete et je poccede un props {props.name}
      <Stack direction="horizontal" gap={2}>
        <Button variant="primary"> Click Me </Button>
        <Button as="a" variant="primary">
          Button as link
        </Button>
        <Button as="a" variant="success">
          Button as link
        </Button>
      </Stack>
      <Button >sdjhsdjhhsjdhjsh</Button> */}
    </div>
  );
}

export default Header;