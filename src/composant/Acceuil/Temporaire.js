import React, { useState, useEffect } from 'react';
import Header from '../Header'
import Footer from '../Footer'
import { Qcm } from '../../qcm';
// import  
function Temporaire(props) {
    // eslint-disable-next-line no-unused-vars
    const [nomNiveau,setnomNiveau] =useState(["Facile", "Intermediaire", "Difficile"])
    // eslint-disable-next-line no-unused-vars
    const [classe,SetClasse]= useState(["Reseaux", "Programmation"])
    // eslint-disable-next-line no-unused-vars
    const [chargement, setChargement] = useState(false)
    // eslint-disable-next-line no-unused-vars
    const [displaid, setSisplaid] = useState(false)
    // eslint-disable-next-line no-unused-vars
    const [niveau, setNivaeu] = useState(0)
    // eslint-disable-next-line no-unused-vars
    const [numSpecialite,setNumSpecialite] =useState(0)
    const [questionparniveau, setQuestionparniveau] = useState([])

    const rechagerQuestion = (list, c) => {
        // const c = 
        const questionsrep = (Qcm.quiz[c])[list]
        // console.log(questionsrep);
        const questions = questionsrep.map(({ reponse, ...reste }) => reste)
        setQuestionparniveau(questions)
    }
    useEffect(() => {
        rechagerQuestion(nomNiveau[niveau], classe[numSpecialite])



// console.log(questionparniveau);

        setTimeout(() => {
            setChargement(true);
        }, 5000);
    },[questionparniveau,nomNiveau,niveau,classe,numSpecialite])


    if (!chargement) {
        return (
            <div>
                <Header>

                    <div className="text-end row">
                        <span className="fw-bold  col-5">Mr/Mme AZEYE </span>
                        {/* <button type="button" className="btn btn-warning ms-2 text-dark me-2" id='activeModals'>Logout</button> */}
                        <div className=" form-switch col-7">
                            <input className="form-check-input me-1 " onChange={1} style={{ width: "40%", height: "50%" }} type="checkbox" role="switch" id="flexSwitchCheckChecked" />
                            <label className="form-check-label " htmlFor="flexSwitchCheckChecked"> Déconnexion</label>
                        </div>
                    </div>

                </Header>
                <div className="container mt-4 mb-5 d-flex justify-content-center align-items-center">
                    <div className="row text-primary ">
                        {/* <button class="btn btn-primary" type="button" disabled> */}
                        <span className="spinner-grow spinner-border-sm cv" style={{ width: "10rem", height: "10rem" }} role="status" aria-hidden="true"></span>
                        {/* </button> */}
                    </div>
                    <span className="fs-1 m-4">Chargement...</span>
                </div>
                <Footer></Footer>
            </div>
        )
    }
    return (
        <div>
            <Header>

                <div className="text-end row">
                    <span className="fw-bold  col-5">Mr/Mme AZEYE </span>
                    {/* <button type="button" className="btn btn-warning ms-2 text-dark me-2" id='activeModals'>Logout</button> */}
                    <div className=" form-switch col-7">
                        <input className="form-check-input me-1 " onChange={1} style={{ width: "40%", height: "50%" }} type="checkbox" role="switch" id="flexSwitchCheckChecked" />
                        <label className="form-check-label " htmlFor="flexSwitchCheckChecked"> Déconnexion</label>
                    </div>
                </div>

            </Header>


            <div className="container mt-4 mb-5 ">
                <div className="row justify-content-between">
                    <div className="col-4  ">
                        <span className="  fs-5">Spécialitée</span>
                        <select className="form-select form-select-lg mb-3" onChange={1} aria-label=".form-select-lg example">
                            <option value="0" defaultValue>Reseaux</option>
                            <option value="1">Programmation </option>
                            <option value="2">Système d'information </option>
                            <option value="3">Système d'exploitation</option>
                            <option value="4">Base de Données</option>
                        </select></div>
                    {/* <div className=""></div> */}
                    <div className="col-3 fs-5 text-center" >Question : <span className='bold'>{1}/10</span> </div>
                    <div className="col-5 fs-5 text-end" >Niveau : </div>
                </div>
                <span className="">Votre Progression</span>
                <div className="progress mt-1" style={{ height: "2px" }}>
                    <div className="progress-bar" role="progressbar" aria-label="Example 1px high" style={{ width: "25%" }} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div className="border border-primary br-5 bg-dark rounded-3  bg-opacity-75  mt-2 mb-4" >
                    <div className="progress-bar bg-success rounded-3" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style={{ width: "25%", height: '25px' }}> <span className='st'>{25}%</span> </div>
                </div>

                <div className="row mb-4 ">
                    <div className="col-12 bg-light fs-4">
                        {/* hjfhjdhfjdhfhj {this.state.numQuestion + 1}: {this.state.question} */}
                    </div>
                </div>
                <div className="row">

                    <div className="col-1 ">
                        {/* 111111111111111111 */}
                    </div>


                    <div className="col-10 ">
                        <div className="container row g-2">

                            {/* {opt} */}
                        </div>
                    </div>
                    <div className="col-1 ">
                        {/* 111111111111111111 */}
                    </div>

                    <div className="col-6 mt-4">
                    </div>
                    <div className="col-6 ms-auto mt-4 me-auto align-items-start">
                        <button type="button" className="btn btn-success  fs-3 p-3" disabled={!displaid} onClick={""} >   {displaid === 100 ? "Continuer" : "Suivant"}</button>
                    </div>
                </div>


            </div>
            <Footer></Footer>
        </div>

    );
}

export default Temporaire;