import React, { Component } from 'react';
import Header from '../Header'
import Footer from '../Footer'
import { Navigate, Link } from 'react-router-dom'
import { Qcm } from '../../qcm';
import firebase from '../../firebase';
import 'firebase/firestore';
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
toast.configure()
// import { useLocation } from "react-router-dom";

// const location = useLocation();
// import { redirect } from 'react-router'
// import PropTypes from 'proptypes';


class index extends Component {
    constructor(props) {
        super(props);
        // console.log(toast);
        console.log(props);
        this.handelchange = this.handelchange.bind(this);
        this.handeladvance = this.handeladvance.bind(this)
        this.handeldeconnexion = this.handeldeconnexion.bind(this)
        this.handelSelectchange = this.handelSelectchange.bind(this)
        this.handelPopupClose = this.handelPopupClose.bind(this)
        this.handeladvanceLevel = this.handeladvanceLevel.bind(this)
        this.handelPopupschow = this.handelPopupschow.bind(this)
        this.state = {
            classe: ["Reseaux", "Programmation"],
            numSpecialite: 0,
            nomNiveau: ["Facile", "Intermediaire", "Difficile"],
            niveau: 0,
            pouc: 10,
            selc: false,
            rec: null,
            options: [],
            numQuestion: 0,
            question: "",
            questionparniveau: [],
            logded: false,
            point: 0,
            reponsech: "",
            selected: true,
            activepopup: "d-none",
            activecontinuer: true,
            spinnerButton: true
        };

    }
    c = (style, ec) => {
        if (style === 0) {
            toast.warn(ec, {
                position: 'top-right',
                autoClose: 1500,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: false
            })
        } else
            if (style === 1) {
                toast.success(ec, {
                    position: 'top-right',
                    autoClose: 2000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: false
                });
            } else {
                toast.info(ec, {
                    position: 'top-right',
                    autoClose: 2000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: false
                });
            }
    }
    chercherReponseQeustion = (idQuestion, reponse) => {
        // console.log("numero specialite"+this.state.numSpecialite);
        const questionsrep = ((Qcm.quiz[this.state.classe[this.state.numSpecialite]])[this.state.nomNiveau[this.state.niveau]])[idQuestion]
        console.log(questionsrep.reponse === reponse);
        return questionsrep.reponse === reponse;

    }
    handelPopupClose() {
        this.handeldeconnexion()
        this.setState({
            activepopup: "d-none"
        })
    }

    rechagerQuestion = (list, c) => {
        // const c = 
        const questionsrep = (Qcm.quiz[c])[list]
        console.log(questionsrep);
        const questions = questionsrep.map(({ reponse, ...reste }) => reste)
        this.setState({
            questionparniveau: questions,

        })
    }

    componentDidMount() {
        // toast("vbvvb")
        firebase
            .firestore()
            .collection("Quiz")
            .onSnapshot((snapshot) => {
                const search = snapshot.docs.map((doc) => ({
                    id: doc.id,
                    ...doc.data()
                }))
                console.log(search);
            })
        console.log(" componentDidMount");
        // console.log(location);
        this.rechagerQuestion(this.state.nomNiveau[this.state.niveau], this.state.classe[this.state.numSpecialite])
        setTimeout(() => {
            this.c(2, "welcome To Bertho-Quizz")
            this.setState({
                logded: true
            })

        }, 2000);
    }
    componentDidUpdate(prevProps, prevState) {

        console.log("componentDidUpdate");
        if (this.state.questionparniveau !== prevState.questionparniveau) {

            this.setState({
                question: this.state.questionparniveau[this.state.numQuestion].question,
                options: this.state.questionparniveau[this.state.numQuestion].options

            })
        }
        // setTimeout(() => {
        //     // this.c(2, "welcomenbsndsgdhghdh To Bertho-Quizz")
        //     this.setState({
        //         logded: true
        //     })

        // }, 2000);
        // this.activationDeactivationSelection()
    }
    //  c = useNavigate()
    handeldeconnexion() {
        this.setState({
            rec: '/'
        })
    }
    // changement de specialite
    handelSelectchange(e) {
        console.log(e.target.selectedOptions[0].value);

        this.setState({
            numSpecialite: e.target.selectedOptions[0].value,
            niveau: 0,
            numQuestion: 0,
            pouc: 10,
            // logded:false

        })
        this.rechagerQuestion(this.state.nomNiveau[0], this.state.classe[e.target.selectedOptions[0].value])
    }
    // methode appeler lors de la selection d'une option
    handelchange(e) {
        console.log(e.target.value);
        // e.target.classList.remove("btn-outline-primary")
        // e.target.classList.add("btn-primary")
        if (this.state.pouc === 100) {
            this.setState({
                // pouc: 10,
                selc: true,
                reponsech: e.target.value,

            })
        } else {
            this.setState({
                // pouc: this.state.pouc + 10,
                selc: true,
                reponsech: e.target.value,

            })
        }
        // console.log(this.state.selecteds);

    }
    handelPopupschow() {
        this.setState({
            activepopup: "d-block"
        })
    }
    handeladvanceLevel() {

        this.setState({
            pouc: 10,
            numQuestion: 0,
            niveau: this.state.niveau + 1,
            activepopup: "d-none",
            activecontinuer: true,
            point: 0
        })
        this.rechagerQuestion(this.state.nomNiveau[this.state.niveau + 1], this.state.classe[this.state.numSpecialite])
        // this.c(1, 'Niveau Facile terminé vous avez ' + (this.state.point));
    }
    // methode appeller lors de l'avancement
    handeladvance() {
        this.setState({
            selc: false,
            selected: true,
            spinnerButton: false
        })
        setTimeout(() => {

            if (this.state.numQuestion + 1 === 10) {

                if (this.chercherReponseQeustion(this.state.numQuestion, this.state.reponsech)) {

                    this.c(1, 'Bravo!! vous avez  +1 ✔✔✔ !!' + (this.state.point + 1));
                    this.setState(prevState => ({
                        point: prevState.point + 1,
                        activecontinuer: false
                    }))
                    // console.log(this.state.numQuestion);
                } else {
                    this.c(0, 'Oups!! vous avez  +0  ');
                }
                // redirect('/')


            } else {
                if (this.chercherReponseQeustion(this.state.numQuestion, this.state.reponsech)) {

                    this.c(1, 'Bravo!! vous avez  +1 ✔✔✔ ' + (this.state.point + 1));
                    this.setState(prevState => ({
                        point: prevState.point + 1
                    }))
                    // console.log(this.state.numQuestion);
                } else {
                    this.c(0, 'Oups!! vous avez  +0  ');
                }
                this.setState({
                    pouc: this.state.pouc + 10,
                    numQuestion: this.state.numQuestion + 1,
                    question: this.state.questionparniveau[this.state.numQuestion + 1].question,
                    options: this.state.questionparniveau[this.state.numQuestion + 1].options
                })
                // console.log(this.state.numQuestion);
            }

            this.setState({
                spinnerButton: true
            })

        }, 500);
    }
    // componentWillMount() {

    // }

    // componentDidMount() {

    // }


    render() {

        const questionlist = ((Qcm.quiz[this.state.classe[this.state.numSpecialite]])[this.state.nomNiveau[this.state.niveau]]).map((question, index) => {
            return (<div className='pt-1 ps-3  border border-dark  border-end-0 border-top-0' key={index}>
                <div className="row  ">
                    <p className="text-truncate col-4  fst-italic " style={{ 'maxWidth': "100%" }}>{index + 1}: {question.question} </p>
                    <span className='col-8 bg-ligth fw-bolder rounded-3  '> : {question.reponse}</span>

                </div>
            </div>)
        })
        const opt = this.state.options.map((option, index) => {
            return (
                <div key={index}>
                    <input type="radio" className="btn-check " value={option} name="options-outlined" id={index} autoComplete="off" onClick={this.handelchange} />

                    <label className="btn btn-outline-primary fs-5 text-start d-flex justify-content-between align-items-start" htmlFor={index}>
                        <span className="badge ms-2 bg-secondary rounded-pill align-items-center">{index + 1}</span>
                        <div className="ms-4 me-auto">
                            <div className=" text-break">{option} </div>
                        </div>
                    </label>
                    {/* <div className="input-group ">

                        <button value={option} id={index} onClick={this.handelchange} className="fs-3 form-control btn-outline-primary text-start" placeholder="Input group example" aria-label="Input group example" aria-describedby="btnGroupAddon" >
                            <div className="input-group-text btn-outline-primary badge ms-2 bg-secondary rounded-pill align-items-center" id="btnGroupAddon">{index}</div>
                            <span className="ms-4">{option}</span>
                        </button>
                    </div> */}
                </div>
            )
        })
        if (this.state.rec) {
            return <Navigate to={this.state.rec}></Navigate>
        }
        if (!this.state.logded) {
            return (
                <div>
                    <Header>

                        <div className="text-end row">
                            <span className="fw-bold  col-5">Mr/Mme AZEYE </span>
                            {/* <button type="button" className="btn btn-warning ms-2 text-dark me-2" id='activeModals'>Logout</button> */}
                            <div className=" form-switch col-7">
                                <input className="form-check-input me-1 " onChange={this.handeldeconnexion} style={{ width: "40%", height: "50%" }} type="checkbox" role="switch" id="flexSwitchCheckChecked" />
                                <label className="form-check-label " htmlFor="flexSwitchCheckChecked"> Déconnexion</label>
                            </div>
                        </div>

                    </Header>
                    <div className="container mt-4 mb-5 d-flex justify-content-center align-items-center">
                        <div className="row  justify-content-center align-items-center">
                            {/* <button class="btn btn-primary" type="button" disabled> */}
                            <span className="spinner-grow spinner-border-sm text-primary cv" style={{ width: "10rem", height: "10rem" }} role="status" aria-hidden="true"></span>
                            <span className="spinner-grow spinner-border-sm text-success cv" style={{ width: "10rem", height: "10rem" }} role="status" aria-hidden="true"></span>
                            <span className="spinner-grow spinner-border-sm text-warning cv" style={{ width: "7rem", height: "7rem" }} role="status" aria-hidden="true"></span>
                            {/* <span className="spinner-grow spinner-border-sm cv" style={{ width: "5rem", height: "5rem" }} role="status" aria-hidden="true"></span> */}
                            {/* <span className="spinner-grow spinner-border-sm text-secondary cv" style={{ width: "7rem", height: "7rem" }} role="status" aria-hidden="true"></span> */}
                            {/* </button> */}
                        </div>

                        <span className="fs-1 m-4">Chargement...</span>
                    </div>
                    <Footer></Footer>
                </div>
            )
        }
        return (
            <div>
                <Header>

                    <div className="text-end row">
                        <span className="fw-bold  col-5">Mr/Mme AZEYE </span>
                        {/* <button type="button" className="btn btn-warning ms-2 text-dark me-2" id='activeModals'>Logout</button> */}
                        <div className=" form-switch col-7">
                            <input className="form-check-input me-1 " onChange={this.handeldeconnexion} style={{ width: "40%", height: "50%" }} type="checkbox" role="switch" id="flexSwitchCheckChecked" />
                            <label className="form-check-label " htmlFor="flexSwitchCheckChecked"> Déconnexion</label>
                        </div>
                    </div>

                </Header>


                <div className="container mt-4 mb-5 ">
                    <div className="row justify-content-between">
                        <div className="col-4  ">
                            <span className="  fs-5">Spécialitée</span>
                            <select className="form-select form-select-lg mb-3" onChange={this.handelSelectchange} aria-label=".form-select-lg example">
                                <option value="0" defaultValue>Reseaux</option>
                                <option value="1">Programmation </option>
                                <option value="2">Système d'information </option>
                                <option value="3">Système d'exploitation</option>
                                <option value="4">Base de Données</option>
                            </select></div>
                        {/* <div className=""></div> */}
                        <div className="col-3 fs-5 text-center" >Question : <span className='bold'><b>{this.state.numQuestion + 1}/10 </b></span> </div>
                        <div className="col-5 fs-5 text-end" >Niveau :<b> {this.state.nomNiveau[this.state.niveau]} </b></div>
                    </div>
                    <span className="">Votre Progression</span>
                    <div className="progress mt-1" style={{ height: "2px" }}>
                        <div className="progress-bar" role="progressbar" aria-label="Example 1px high" style={{ width: this.state.pouc + "%" }} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <div className="border border-primary br-5 bg-dark rounded-3  bg-opacity-75  mt-2 mb-4" >
                        <div className="progress-bar bg-success rounded-3" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style={{ width: this.state.pouc + "%", height: '25px' }}> <span className='st'>{this.state.pouc}%</span> </div>
                    </div>

                    <div className="row mb-4 ">
                        <div className="col-12 bg-light fs-4">
                            {this.state.numQuestion + 1}: {this.state.question}
                        </div>
                    </div>
                    <div className="row">

                        <div className="col-1 ">
                            {/* 111111111111111111 */}
                        </div>


                        <div className="col-10 ">
                            <div className="container row g-2">

                                {opt}
                            </div>
                        </div>
                        <div className="col-1 ">
                            {/* 111111111111111111 */}
                        </div>

                        <div className="col-6 mt-4">
                        </div>
                        <div className="col-6 ms-auto mt-4 me-auto align-items-start">
                            {this.state.activecontinuer ?
                                this.state.spinnerButton ?
                                    <button type="button" className="btn btn-success  fs-3 p-3" disabled={!this.state.selc} onClick={this.handeladvance} > Suivant  </button>
                                    :
                                    <button className="btn btn-success fs-3 p-3" type="button" disabled>
                                        <span className="spinner-border spinner-border-sm" style={{ width: "2rem", height: "2rem" }} role="status" aria-hidden="true"></span>
                                        <span className=""> Loading...</span>
                                    </button>
                                :
                                <button type="button" className="btn btn-success  fs-3 p-3" onClick={this.handelPopupschow} > Valider  </button>

                            }
                        </div>
                    </div>


                </div>
                <Footer></Footer>

                <div className={ // eslint-disable-next-line no-useless-concat
                    this.state.activepopup + " " + "modal  modal-signin    bg-none py-0"} tabIndex="-1" role="dialog" id="modalSignin">
                    <div className="modal-dialog " role="document">
                        <div className="modal-content rounded-5 shadow ">
                            <div className="modal-header p-3 pb-2 border-bottom-0 border border-success">

                                <h3 className="fw-bold mb-0">Vos resultats  </h3>
                                <h3 className="ms-4 mb-0 text-info " >Score: {this.state.point} /10</h3>
                                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" onClick={this.handelPopupClose} id='close'></button>
                            </div>

                            <div className="modal-body border border-info  pt-2">
                                {questionlist}


                                <div className="row justify-content-between mt-1">
                                    <div className="col-4  ">
                                        <Link to="/" className="btn btn-outline-danger me-2 container md-5">
                                            Quitter
                                        </Link>                                    </div>
                                    <div className="col-5 fs-5 text-end" >
                                        <button type="button" className="btn btn-info me-2 container md-5" onClick={this.handeladvanceLevel} > continuer  </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    activationDeactivationSelection = () => {
        document.getElementById("0") !== null ?
            document.getElementById("0").addEventListener('click', () => {
                console.log("okkkk 1");
                document.getElementById("0").classList.remove("btn-outline-primary")
                document.getElementById("1").classList.remove("btn-primary")
                document.getElementById("2").classList.remove("btn-primary")
                document.getElementById("3").classList.remove("btn-primary")

                document.getElementById("0").classList.add("btn-primary")
                document.getElementById("1").classList.add("btn-outline-primary")
                document.getElementById("2").classList.add("btn-outline-primary")
                document.getElementById("3").classList.add("btn-outline-primary")
            })
            :
            console.log(document.getElementById("1"));
        ;
        document.getElementById("1") !== null ?
            document.getElementById("1").addEventListener('click', () => {
                document.getElementById("1").classList.remove("btn-outline-primary")
                document.getElementById("0").classList.remove("btn-primary")
                document.getElementById("2").classList.remove("btn-primary")
                document.getElementById("3").classList.remove("btn-primary")

                document.getElementById("1").classList.add("btn-primary")
                document.getElementById("0").classList.add("btn-outline-primary")
                document.getElementById("2").classList.add("btn-outline-primary")
                document.getElementById("3").classList.add("btn-outline-primary")
                console.log("okkkk 1");
            })
            :
            console.log(document.getElementById("1"));
        ;
        document.getElementById("2") !== null ?
            document.getElementById("2").addEventListener('click', () => {
                document.getElementById("2").classList.remove("btn-outline-primary")
                document.getElementById("1").classList.remove("btn-primary")
                document.getElementById("0").classList.remove("btn-primary")
                document.getElementById("3").classList.remove("btn-primary")

                document.getElementById("2").classList.add("btn-primary")
                document.getElementById("1").classList.add("btn-outline-primary")
                document.getElementById("0").classList.add("btn-outline-primary")
                document.getElementById("3").classList.add("btn-outline-primary")
                console.log("okkkk 2");
            })
            :
            console.log(document.getElementById("1"));
        ;
        document.getElementById("3") !== null ?
            document.getElementById("3").addEventListener('click', () => {
                document.getElementById("3").classList.remove("btn-outline-primary")
                document.getElementById("1").classList.remove("btn-primary")
                document.getElementById("2").classList.remove("btn-primary")
                document.getElementById("0").classList.remove("btn-primary")

                document.getElementById("3").classList.add("btn-primary")
                document.getElementById("1").classList.add("btn-outline-primary")
                document.getElementById("2").classList.add("btn-outline-primary")
                document.getElementById("0").classList.add("btn-outline-primary")
                console.log("okkkk 3");
            })
            :
            console.log(document.getElementById("1"));
        ;
    }
}


// index.propTypes = {
//    router: React.PropTypes.shape(){
//         push : React.PropTypes.func.isRequired
//     }
// };


export default index;