import React from 'react';
import { useState } from 'react';
import firebase from '../firebase'
import { toast } from 'react-toastify'
import { useNavigate } from 'react-router-dom'
import 'react-toastify/dist/ReactToastify.css';
toast.configure()

function Signin(props) {
    const nav = useNavigate()
    // console.log(nav);
    // eslint-disable-next-line no-unused-vars
    const redirection = (user) => {
        nav('/acceuil', { state: { message: "Failed to submit form" } });
    }

    const db = firebase.firestore().collection('Personne');
    const firebas = firebase.auth();
    const handelsubmit = (e) => {
        e.preventDefault();

        firebas.createUserWithEmailAndPassword(email, Password)
            .then(user => {
                c(0, "Compte creee Avec succes");
                db.add({
                    Nom: name,
                    prenom: name,
                    email: email,
                    password: Password
                })
                    .then(v => {
                        // props.history.push('/welcome'
                    })
                    .catch(err => {
                        const errorCode = err.code;
                        if (errorCode === "auth/network-request-failed") {
                            setError("veillez vous connecter sur Internet")
                            c(0, "veillez vous connecter sur Internet");

                            console.log('errorCode')
                        }
                        if (errorCode === "auth/email-already-in-use") {
                            setError("votre addresse email existe deja")
                            c(0, "votre addresse email existe deja");
                            // console.log('errorCode')
                        }
                        console.log(err)
                    });
                // setlogdata({ ...data })
                // props.history.push('/welcome')   ;
                // Auth.login();
                setSigndata({ ...data });
                redirection(user)
                console.log(user);
                console.log('login okkkkkkkkkk');
                console.log('okkkkkkk');
            })
            .catch(error => {
                console.log(error.code)
                const errorCode = error.code;
                // const errorMessage = error.message;
                if (errorCode === "auth/network-request-failed") {
                    setError("veillez vous connecter sur Internet")
                    c(0, "veillez vous connecter sur Internet");

                    console.log('errorCode')
                }
                if (errorCode === "auth/email-already-in-use") {
                    setError("votre addresse email existe deja")
                    c(0, "votre addresse email existe deja");
                    // console.log('errorCode')
                }
                // console.log(errorCode)
                // console.log(errorMessage)

            })
        // console.log(e)
    }
    const c = (style, ec) => {
        if (style === 0) {
            toast.warn(ec, {
                position: 'top-right',
                autoClose: 3500,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: false
            })
        } else
            if (style === 1) {
                toast.success(ec, {
                    position: 'top-right',
                    autoClose: 2000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: false
                });
            } else {
                toast.info(ec, {
                    position: 'top-right',
                    autoClose: 2000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: false
                });
            }
    }
    const handelchange = (e) => {
        // setSigndata()
        setSigndata(({ ...signdata, [e.target.id]: e.target.value }))
        console.log(name)
    }
    const data = {
        name: '',
        email: '',
        Password: '',
        confirmationpass: ''
    }
    const [signdata, setSigndata] = useState(data)
    const [error, setError] = useState('')
    const { name, email, Password, confirmationpass } = signdata

    return (
        <div className="modal modal-signin  d-none bg-none py-0" tabIndex="-1" role="dialog" id="modallogin">
            <div className="modal-dialog" role="document">
                <div className="modal-content rounded-5 shadow">
                    <div className="modal-header p-5 pb-4 border-bottom-0">

                        <h3 className="fw-bold mb-0">Creer un Compte </h3>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" id='closesign'></button>
                    </div>

                    <div className="modal-body p-5 pt-0">
                        <form className="" id="signin" action='/dfddfdf' method='post' onSubmit={handelsubmit}>
                            <div className="form-floating mb-1">
                                <input type="text" className="form-control rounded-4" onChange={handelchange} id="name" value={name} minLength={2} maxLength={15} required placeholder="name@example.com" />
                                <label htmlFor="floatingInput">votre Pseudo</label>
                            </div>
                            <div className="form-floating mb-1">
                                <input type="email" className="form-control rounded-4" id="email" onChange={handelchange} value={email} required placeholder="name@example.com" />
                                <label htmlFor="floatingInput">Email address</label>
                            </div>

                            <div className="form-floating mb-2">
                                <input type="password" className="form-control rounded-4" id="Password" onChange={handelchange} value={Password} minLength={8} maxLength={8} required placeholder="Password" />
                                <label htmlFor="Password">Password</label>
                                <div className="invalid-feedback">
                                    Le mot de pass doit avoir au moins 8 caractère.
                                </div>
                            </div>
                            <div className="form-floating mb-2">
                                <input type="password" className="form-control rounded-4" id="confirmationpass" onChange={handelchange} value={confirmationpass} minLength={8} maxLength={8} required placeholder="Password" />
                                <label htmlFor="confirmationpass">confirmation Password</label>
                                <div className="invalid-feedback">
                                    Le mot de pass doit avoir au moins 8 caractère.
                                </div>
                            </div>
                            <button className="w-100 mb-2 btn btn-lg rounded-4 btn-primary" type="submit" id='submitButtom1'>Continuer</button>

                            <small className="text-muted">En cliquant sur <b>Connexion</b> vous accepter les conditions d'utilisation.</small> <br></br>
                            <span className='text-warning'> {error}</span>
                            <hr className="my-4" />
                            {/* <h2 className="fs-5 fw-bold mb-3"> Ou se connecter avec :</h2>
                                <button className="w-100 py-2 mb-2 btn btn-outline-dark rounded-4" type="submit">
                                    Se connecter avec Google
                                </button>
                                <button className="w-100 py-2 mb-2 btn btn-outline-primary rounded-4" type="submit">
                                    Se connecter avec Facebook  
                                </button>
                                <button className="w-100 py-2 mb-2 btn btn-outline-secondary rounded-4" type="submit">
                                    Se connecter avec GitHub
                                </button> */}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Signin;