import React from 'react';

function Footer(props) {
    return (
        <div>
            <div className="b-example-divider"></div> 

            <div className="container">
                <footer className="py-3 my-4">
                    <ul className="nav justify-content-center border-bottom pb-3 mb-3">
                        <li className="nav-item"><a href="#f" className="nav-link px-2 text-muted">Projet Développé par AZEYE Romeo Bertho</a></li>
                        <li className="nav-item"><a href="mailto:berthoazeye21@gmail.com?subject=Messages de soutient a berthoazeye21@gmail.com&body=Bonjour Mr Bertho>" className="nav-link px-2 font-monospace">Laissez moi un mail</a></li>

                    </ul>
                    <p className="text-center text-muted">&copy; 2022 Bertho, Fundation</p>
                </footer>
            </div>
        </div>
    );
}

export default Footer;