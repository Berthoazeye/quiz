import './App.css';
import Home from './composant/Home'
import Text from './composant/text'
import Acceuil from './composant/Acceuil';
import { connect } from "react-redux";
// import logo from './logo.svg';
import { BrowserRouter, Route, Routes as Switch } from 'react-router-dom'

function App(props) {
  // console.log(props);
  return (

    <BrowserRouter >

      <Switch>
        <Route exact path="/" element={<Home value={"i"} />} />
        <Route path="/acceuil" element={<Acceuil />} />
        <Route path="/text" element={<Text />} />
        {/* <Route exact path="/recovery-password" element={<RecoveryPassword/>}/>
          <Route path="*" element={<NotFound/>}/> */}
      </Switch>

    </BrowserRouter >




  );
}

export default App;
